# minify-js.fish

# Copyright 2023 cnngimenez

# Author: cnngimenez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#
# Usage: scripts/minify-js.fish
#

set -l jsfiles \
js/passwordhasher/sha1.js \
js/passwordhasher/passhashcommon.js \
js/passwordhasher/common.js \
js/debug.js \
js/storage.js \
js/lsttags.js \
js/export.js \
js/index.js

uglifyjs $jsfiles --source-map -o js/hashit.min.js
