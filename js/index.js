/*
  Copyright (C) 2023  cnngimenez

  index.js

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/**
 The main navigator which has the pages.

 This is setted when the page has been loaded.
 */
var main_navigator = null;

/**
 List of policies (types of passwords).

 They are orderer from the stronger to the weaker type.
 */
const lst_policies = [
    {
        id: 'alfanumsym',
        name: 'Letras, números y símbolos',
        seed: null,
        length: 12,
        strength: -1,
        custom: {
            d: true,
            p: true,
            m: true,
            r: false,
            only_digits: false
        }
    },
    {
        id: 'alfanum',
        name: 'Letras y números',
        seed: null,
        length: 12,
        strength: -1,
        custom: {
            d: true,
            p: false,
            m: true,
            r: true,
            only_digits: false
        }
    },
    {
        id: 'pin10',
        name: 'PIN de 10 dígitos',
        seed: null,
        length: 10,
        strength: -1,
        custom: {
            d: false,
            p: false,
            m: false,
            r: true,
            only_digits: true
        }
    },
    {
        id: 'pin8',
        name: 'PIN de 8 dígitos',
        seed: null,
        length: 8,
        strength: -1,
        custom: {
            d: true,
            p: false,
            m: false,
            r: false,
            only_digits: true
        }
    },
    {
        id: 'pin4',
        name: 'PIN de 4 dígitos',
        seed: null,
        length: 4,
        strength: -1,
        custom: {
            d: true,
            p: false,
            m: false,
            r: false,
            only_digits: true
        }
    }
];

/**
 This is the default configuration.
 */
const default_config = {
    tag: '',
    bump: 0,
    policy: {
        seed: null,
        length: 12,
        custom: {
            d: true, // at least one digit
            p: true, // at least one punctuation
            m: true, // mixed case
            r: false,  // no special characters
            only_digits: false
        }
    },
};

/**
 Configuration used to generate password hints.

 The tag is not provided because is the same as the password.
 */
const hint_config = {
    bump: 0,
    policy: {
        seed: null,
        length: 2,
        custom: {
            d: true, // at least one digit
            p: false, // at least one punctuation
            m: true, // mixed case
            r: true,  // no special characters
            only_digits: false
        }
    },
};

function generate_hash(config, masterpass) {
    return PassHashCommon.generateHashWord (
        config.tag,
        masterpass,
        config.policy.length,
        config.policy.custom.d, // require digits
        config.policy.custom.p, // require punctuation
        config.policy.custom.m, // require mixed case
        config.policy.custom.r, // no special characters
        config.policy.custom.only_digits); // only digits

}

function generate_password_hint(password) {
    let config = {tag: password, ...hint_config};
    return generate_hash(config, password);
}

// --------------------------------------------------

function get_tag_from_page() {
    let elt = document.querySelector('#sitetag');
    let elt_userlabel = document.querySelector('#usertag-label');
    let elt_bumplabel = document.querySelector('#bumptag-label');

    return elt_userlabel.innerText + elt.value + elt_bumplabel.innerText;
}

function get_policy_from_page() {
    var elt = document.querySelector('#passtype');
    var policyid = elt.attributes.data.value;
    var selected_policy = lst_policies.find( (policy) => {
        return policy.id == policyid;
    });

    if (selected_policy == null || selected_policy == undefined) {
        return lst_policy['alfanumsym'];
    } else {
        return selected_policy;
    }
}

function get_config_from_page() {
    var myconfig = default_config;
    myconfig.tag = get_tag_from_page();
    myconfig.policy = get_policy_from_page();
    
    return myconfig;
}

function get_master_pass_from_page() {
    var elt = document.querySelector('#masterpass');
    return elt.value;
}

function get_passtype_from_page() {
    var elt = document.querySelector('#passtype');
    return elt.attributes.data.value;
}

/**
 Show the suggested tag at the page.
 
 @param tag String The tag to suggest. If it is empty or null,
            hide the suggestion.
*/
function show_suggest_tag(tag) {
    let elt = document.querySelector('#tag-suggestion');
    let label = document.querySelector('#tag-suggested');
    if (tag == undefined || tag == null || tag == '') {
        elt.style.visibility = 'collapse'
        label.innerText='';
    } else {
        elt.style.visibility = ''
        label.innerText = '¿Seguro no quiere utilizar el tag "' + tag + '"?'
        elt.setAttribute('data', tag);
    }
}

function add_type(elt, name, jsonname) {
    var itemelt = document.createElement('ons-list-item');
    itemelt.setAttribute('tappable', true);
    itemelt.innerText = name;    
    itemelt.setAttribute('data', jsonname);
    itemelt.addEventListener('click', type_clicked);
    elt.append(itemelt);
}

function update_passtype_list() {
    var elt = document.querySelector('#lsttypes');
    lst_policies.forEach( (policy) => {
        add_type(elt, policy.name, policy.id);
    });
}

function update_current_policy(policy) {
    var elt = document.querySelector('#passtype');
    elt.setAttribute('data', policy.id);
    elt.innerText = policy.name;    
}

function update_tag() {
    let elt_userinput = document.querySelector('#usertag');    
    let elt_userlabel = document.querySelector('#usertag-label');
    let elt_bumpinput = document.querySelector('#tagbump');
    let elt_bumplabel = document.querySelector('#bumptag-label');
    
    if (elt_userinput.value != "") {
        elt_userlabel.innerText = elt_userinput.value + '@';
    } else {
        elt_userlabel.innerText = '';
    }

    if (elt_bumpinput.value != "" && parseInt(elt_bumpinput.value) > 0) {
        elt_bumplabel.innerText = ':' + elt_bumpinput.value;        
    } else {
        elt_bumplabel.innerText = ''
    }

    update_hash();
    update_tag_suggestion()
}

function update_save_button() {
    let elt = document.querySelector('#saveconfig');

    elt.disabled = get_tag_from_page() == '';    
}

/**
 Update the hash showed to the user.

 Do not show any hashed password if no master-pass was provided.
 */
function update_hash(){
    // Tag is in the config.
    let config = get_config_from_page();
    let master_pass = get_master_pass_from_page();
    var elt = document.querySelector('#hashedpass');
    
    if (master_pass != '') {
        elt.value = generate_hash(config, master_pass);
    } else {
        elt.value = ''
    }
}

function update_hint() {
    let elt = document.querySelector('#masterpass-hint');
    elt.innerText = generate_password_hint(get_master_pass_from_page());
}

/**
 If the tag is an URL or a domain, suggest a simpler tag. 
*/
function update_tag_suggestion() {
    // RegExp from https://regexr.com/3e6m0 and https://stackoverflow.com/questions/3809401/what-is-a-good-regular-expression-to-match-a-url
    const urlregexp = /(http(s)?:\/\/)?(www\.)?([-a-zA-Z0-9@:%._\+~#=]{2,256})\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/
    let tagstr = get_tag_from_page();
    let matches = tagstr.match(urlregexp);

    if (matches == null) {
        show_suggest_tag(null);
    } else {
        show_suggest_tag(matches[4]);
    }
}

/**
 Show the info tab.

 The main page must be visible. See show_main_page();
 */
function show_info_tab() {
    let elt = document.querySelector('#main-tabbar');
    elt.setActiveTab(2);

}

// --------------------------------------------------
// Buttons and HTML Handlers

function toggle_usertag(show) {
    let elt = document.querySelector('#usertag');
    let elt_label = document.querySelector('#usertag-label');
    if (show == undefined || show == null) {
        // Toggle
        if (elt.style.visibility == 'collapse') {
            elt.style.visibility = '';
            update_tag();
        } else {
            elt.style.visibility = 'collapse';
            elt_label.innerText = '';
        }        
    }

    if (show) {
        elt.style.visibility = '';
        update_tag();
    } else {
        elt.style.visibility = 'collapse';
        elt_label.innerText = '';        
    }  
}

function on_usertag_switched() {
    elt = document.querySelector('#usertag-switch');
    toggle_usertag(elt.checked);
    update_hash();    
}

function on_sitetag_changed() {
    update_hash();
    update_save_button();
    update_tag_suggestion();
}

function on_masterpass_changed() {
    update_hash();
    update_hint();
}

function on_use_suggested_tag_clicked() {
    let elt = document.querySelector('#tag-suggestion');
    let tag = elt.attributes.data.value;
    let taginput = document.querySelector('#sitetag');

    taginput.value = tag;
    on_sitetag_changed();
}

function on_tagbump_changed() {
    update_tag();
    // on_sitetag_changed();
}

/* NO NEED... The ons-input with type=number has a plus/minus.
 
function on_more_bump() {
    let elt = document.querySelector('#tagbump');
    if (elt.value == undefined || elt.value == null) {
        elt.value = 0;
    } else {
    elt.value = parseInt(elt.value) + 1;
    }

    on_sitetag_changed();
}

function on_less_bump() {
    let elt = document.querySelector('#tagbump');
    if (elt.value == undefined || elt.value == null) {
        elt.value = 0;
    } else {
        if (elt.value > 0) {
        elt.value = parseInt(elt.value) - 1;
        }
    }

    on_sitetag_changed();
    }
    */

function saveconfig_clicked() {
    var config = get_config_from_page();
    if (config == null || config == undefined) {
        return;
    }
    st_save_config(config, (c) => {
        ons.notification.toast('Tag y configuración guardada.', { timeout: 2000 });
    });
}

function type_clicked(e) {
    var id = e.target.parentElement.attributes.data.value;
    var policy = lst_policies.find( (policy) => {
        return policy.id == id;
    });

    if (policy != null && policy != undefined) {
        update_current_policy(policy);
        update_hash();
    }
}

/**
 Show the indicated popover.
 */
function popover_show(target, id) {
    let elt = document.querySelector(id);
    elt.show(target, {
        animationOptions: {duration: 0.2, delay: 0.4, timing: 'ease-in'}
    });
}

function toggle_showpass(input_id) {
    var elt = document.querySelector(input_id);
    if (elt.attributes.type.value == 'password') {
        elt.setAttribute('type', 'text');
    } else {
        elt.setAttribute('type', 'password');
    }
}

function toggle_showpass_clicked() {
    toggle_showpass('#hashedpass');
}

function copy_pass(input_id) {
    var elt = document.querySelector(input_id);
    navigator.clipboard.writeText(elt.value).then(
        () => {
            ons.notification.toast('Password copied!', { timeout: 2000 });
            debug_log('Password copied successfully.');
        },
        (err) => {
            ons.notification.toast('Password *could not* be copied!', { timeout: 2000 });
            console.error('Password could not be copied. Error: ', err);
        });
}

function copy_pass_clicked() {
    copy_pass('#hashedpass');
}

function assign_handlers() {
    document.addEventListener('prechange', function(event) {
        let elttoolbar = document.querySelector('ons-toolbar .center');
        elttoolbar.innerHTML = event.tabItem.getAttribute('label');

        if (event.tabItem.getAttribute('page') == 'taglist.html'){
            lsttags_update_page();
        }
        if (event.tabItem.getAttribute('page') == 'export.html') {
            export_update_page();
        }
        if (event.tabItem.getAttribute('page') == 'debug.html'){
            debug_update_page();
        }
    });

    toggle_usertag(false);
}

function setup_main_page(){
    update_tag_suggestion();
    update_passtype_list();
    assign_handlers();
    st_open();
}

function on_page_ready() {       
    debug_log('Welcome to Password Hasher!');
    main_navigator = document.getElementById('navigator');
    
    document.addEventListener('init', function (event) {
        var page = event.target;
        if (page.matches('#page-home')) {
            // If home page is not initialized like this, the page
            // would not be loaded before JS uses their inputs.
            setup_main_page();
        }

        if (page.matches('#page-tutorial')) {
            setup_tutorial_page();
        }
    });
    
    if (should_show_tutorial()) {
        show_tutorial();
    } else {
        show_main_page();
    }
}

/**
 Should the tutorial be visible after restarting?

 @return True if the tutorial should be presented at start.
 */
function should_show_tutorial() {
    return localStorage.getItem('show_tutorial') != 'false';
}

/**
 Save if the tutorial should repeat after restarting.

 @param repeat Boolean Should repeat the tutorial?
 */
function save_repeat_tutorial(repeat) {
    localStorage.setItem('show_tutorial', String(repeat));
}

/**
 Tell the main navigator to switch to the main page.

 Also, it setups the main page after loading it.
 */
function show_main_page() {
    return main_navigator.bringPageTop('page-main.html');
}

/**
 Tell the main navigator to switch to the tutorial page.
 */
function show_tutorial() {
    return main_navigator.bringPageTop('page-tutorial.html').then( () => {
        tutorial_show_step(0);
    });
}

// --------------------------------------------------
// Tutorial

function tutorial_show_step(stepnum) {
    let tabbar = document.getElementById('tutorial-tabbar');
    
    if (stepnum >= 0 && stepnum < tabbar.tabs.length) {
        return tabbar.setActiveTab(stepnum);
    }
}

function tutorial_update_step(stepnum) {
    let elt = document.querySelector('svg.tutorial-steps-svg');
    let circles = elt.querySelectorAll('circle')
    let current = elt.querySelector('circle#step' + String(stepnum));

    circles.forEach( (c) => {
        c.style.opacity = 0.5;
    });

    if (current == null) {
        return;
    }        

    current.style.opacity = 1;                     
}

function setup_tutorial_page() {
    let toolbar = document.querySelector('#tutorial-toolbar');
    toolbar.setVisibility(false);

    tutorial_update_step(1);
    
    document.addEventListener('postchange', function(event) {
        // Action to do whenever the user changes the tab page.
        let tabbar = document.querySelector('ons-tabbar#tutorial-tabbar');
        let i = tabbar.getActiveTabIndex();
        
        tutorial_update_step(i + 1);
    });
}

function on_tutorial_next_btn(){    
    let tabbar = document.getElementById('tutorial-tabbar');
    let i = tabbar.getActiveTabIndex();
    
    // For some reason, the tabbar sometimes needs to tell it
    // to use the current tab.
    tutorial_show_step(i).then( () => {
        tutorial_show_step(i + 1);
    });
}

/**
 What to do when the user clicks on the End Tutorial Button.

 Save the option if the user wants to show the tutorial again. Then
 show the main page.
 */
function on_tutorial_end_btn() {
    let chk_repeat = document.querySelector('input#tutorial-again');
    save_repeat_tutorial(chk_repeat.checked);
    
    show_main_page();
}

function on_tutorial_info_clicked() {
    show_main_page().then( () => {
        show_info_tab();
    });
}

ons.ready(on_page_ready);


/*
if (document.readyState !== 'loading') {
    on_page_ready();    
} else {
    document.addEventListener('DOMContentLoaded', on_page_ready);
}
*/
