/*
  Copyright (C) 2023  cnngimenez

  export.js

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/**
 Check if the given config is correct.
 
 @return true when the config object has all fields.
*/
function check_config (data) {
    return data.tag != undefined && typeof(data.tag) == "string";
}

/**
 Check all configs in data.
 */
function check_config_list(data) {
    if (! (data instanceof Array)) {
        return false;
    }
    
    return data.every( (config) => {
        return check_config(config);
    });
}

function export_import_data(data) {
    if (!check_config_list(data)) {
        return false;
    }

    data.forEach( (config) => {
        st_save_config(config);
    });
    
    return true;
}

/*
 Generate the JSON data to use as export.
 
 @return String The JSON data as string.

function export_generate_json() {
    st_get_all(null, set_export_data);
}
*/

// --------------------------------------------------

function get_export_data_from_page() {
    let elt = document.querySelector('#export_data');
    return elt.value;
}

function set_export_data(data) {
    let elt = document.querySelector('#export_data');
    elt.value = JSON.stringify(data);
}

// --------------------------------------------------
// Handlers

function export_add_clicked() {
    let text = document.querySelector('#export_pastetext').value;
    var obj = null;
    try {
        obj = JSON.parse(text);
    } catch {
        ons.notification.toast('Error al interpretar la entrada. Verifique el formato del texto pegado.',
                               {timeout : 2000});
        return false;
    }

    if (export_import_data(obj)) {
        ons.notification.toast('Se importaron sus datos. Corrobore el listado.',
                               {timeout : 2000});
    } else {
        ons.notification.toast('Hubo inconvenientes al importar sus datos.',
                               {timeout : 2000});
    }
}

function export_paste_clicked() {
    try {
        navigator.clipboard.readText().then( (text) => {
            let elt = document.querySelector('#export_data');
            elt.value = text;        
        });
    } catch {
        ons.notification.toast('Hubo problemas al pegar los datos. Intente manualmente.');
    }
    
}

function export_copy_clicked() {
    let jsonstr = get_export_data_from_page();
    navigator.clipboard.writeText(jsonstr).then(
        () => {
            ons.notification.toast('Tags y configuración copiada!',
                                   {timeout: 2000});
        },
        (err) => {
            ons.notification.toast('No se pudo copiar los datos. Intente manualmente.');
        });
}

function export_download_clicked() {
}

function export_update_page() {
    // Update the export data.
    st_get_all(null, set_export_data);
}
