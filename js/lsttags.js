/*
  Copyright (C) 2023  cnngimenez

  lsttags.js

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

var selected_config = null;

function tag_clicked(evt) {
    var tagname = evt.target.parentElement.attributes.data.value;
    st_get_config(tagname, (config) => {
        selected_config = config;
        lsttags_update_selected_config();
        lsttags_update_hash();
    });
}

function lsttag_copy_pass_clicked() {
    copy_pass('#lsttag-hashedpass');
}

function lsttag_toggle_showpass_clicked() {
    toggle_showpass('#lsttag-hashedpass');
}

function lsttag_delete_clicked(evt) {
    evt.stopPropagation();
    
    var tag = "";
    if (evt.target.tagName == 'ONS-BUTTON') {
        // Clicked on the button itself.
        tag = evt.target.attributes.data.value;
    } else {
        // Clicked on the icon inside the button.
        tag = evt.target.parentElement.attributes.data.value;
    }
    
    st_delete(tag, (t) => {
        lsttags_update_page();
    });
}

function add_config(config) {
    let elt_lsttags = document.querySelector('#lsttags');

    let itemelt = document.createElement('ons-list-item');
    itemelt.setAttribute('tappable', true);
    itemelt.setAttribute('data', config.tag);
    itemelt.addEventListener('click', tag_clicked);

    let divcenter = document.createElement('div');
    divcenter.setAttribute('class', 'center');
    divcenter.innerText = config.tag;
    
    let divright = document.createElement('div');
    divright.setAttribute('class', 'right');

    let delbutton = document.createElement('ons-button');
    delbutton.addEventListener('click', lsttag_delete_clicked);
    delbutton.setAttribute('data', config.tag);
    delbutton.setAttribute('aria-label', 'Delete tag named ' + config.tag);
    let delicon = document.createElement('ons-icon');
    delicon.setAttribute('icon', 'md-delete');
    delbutton.append(delicon);
    
    divright.append(delbutton);
    
    itemelt.append(divcenter);
    itemelt.append(divright);
    elt_lsttags.append(itemelt);
}

// --------------------------------------------------
function lsttags_get_masterpass_from_page() {
    let elt = document.querySelector('#lsttag-masterpass');
    return elt.value;
}

// --------------------------------------------------

function lsttags_update_selected_config() {
    let elt = document.querySelector('#selectedtag');
    if (selected_config == undefined || selected_config == null) {
        elt.innerText = "Ninguno"
    } else {
        elt.innerText = selected_config.tag;
    }
}

/**
 Update the hash showed to the user.

 Do not show any hashed password if no master-pass was provided.
 */
function lsttags_update_hash() {
    if (selected_config == null) {
        return;
    }

    let elt = document.querySelector('#lsttag-hashedpass');    
    let master_pass = lsttags_get_masterpass_from_page();
    
    if (master_pass != '') {
        elt.value = generate_hash(selected_config, master_pass);
    } else {
        elt.value = '';
    }
}

function add_all_tags(list) {
    list.forEach( (config) => {
        add_config(config);
    });
}

/**
 Update the list of tags on the page.
 */
function lsttags_update_list() {
    let elt_lsttags = document.querySelector('#lsttags');
    elt_lsttags.innerHTML = '';
    
    st_get_all(null, add_all_tags);
}

function lsttags_update_page(){
    debug_log('Update lsttags page');
    let elt_tagamount = document.querySelector('#tagamount');
    st_count((result) => {
        elt_tagamount.innerText = result;
    });
    
    lsttags_update_list();
}

/**
 Update the hint text on the page.
 */
function lsttags_update_hint() {
    let elt = document.querySelector('#lsttags-masterpass-hint');
    elt.innerText = generate_password_hint(lsttags_get_masterpass_from_page());
}

function on_lsttag_masterpass_changed() {
    lsttags_update_hash();
    lsttags_update_hint();
}
