/*
  Copyright (C) 2023  cnngimenez

  storage.js

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const dbname = 'tag_storage';
let db;

function st_save_config(config, what_to_do) {
    const object_store = db.transaction([dbname], 'readwrite').objectStore(dbname);
    const request = object_store.add(config);
    request.onsuccess = (evt) => {
        debug_log('IndexDB object store succeded.');
        if (what_to_do != undefined || what_to_do != null) {
            what_to_do(config);
        }
    };
}

function st_get_config(tag, what_to_do) {
    const object_store = db.transaction([dbname], 'readwrite').objectStore(dbname);
    const request = object_store.get(tag);
    request.onsuccess = () => {
        debug_log('IndexDB got a config: ' + JSON.stringify(request.result));
        if (what_to_do != undefined || what_to_do != null) {
            what_to_do(request.result);
        }
    };
}

function st_count(what_to_do) {
    const object_store = db.transaction([dbname], 'readwrite').objectStore(dbname);
    const request = object_store.count();
    request.onsuccess = () => {
        debug_log('IndexDB got count: ' + request.result);
        if (what_to_do != undefined || what_to_do != null) {
            what_to_do(request.result);
        }
    };
}

function st_get_all(limit, what_to_do) {
    const object_store = db.transaction([dbname], 'readwrite').objectStore(dbname);
    const request = object_store.getAll(null, limit);
    request.onsuccess = () => {
        debug_log('IndexDB got all configs.');
        if (what_to_do != undefined || what_to_do != null) {
            what_to_do(request.result);
        }
    };
}

function st_delete(tag, what_to_do) {
    const object_store = db.transaction([dbname], 'readwrite').objectStore(dbname);
    const request = object_store.delete(tag);
    request.onsuccess = () => {
        debug_log('IndexDB deleted: ' + tag);
        if (what_to_do != undefined || what_to_do != null) {
            what_to_do(tag);
        }
    };
}

/**
 Open the InedxedDB.

 @param what_to_do A function to execute when the DB is successfully opened.
 */
function st_open(what_to_do) {
    const open_request = window.indexedDB.open(dbname, 1);
    open_request.onsuccess = (evt) => {
        debug_log('IndexDB: DB opened successfully.');
        db = open_request.result;
        if (what_to_do != undefined || what_to_do != null) {
            what_to_do();
        }
    };
    open_request.onupgradeneeded = (evt) => {
        debug_log('IndexDB: Upgrading DB.');
        
        db = evt.target.result;
        const object_store = db.createObjectStore(dbname, { keyPath: 'tag' });
    };
}
