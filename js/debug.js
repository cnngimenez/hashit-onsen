/*
  Copyright (C) 2023  cnngimenez

  debug.js

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

var debugmsg = [];

/**
 Maintain 100 elements on the array.
 */
function debug_remove_old() {
    if (debugmsg.length > 100) {
        debugmsg = debugmsg.slice(-100);
    }
}

function debug_log(msg) {
    console.log(msg);
    debugmsg.push(msg);
    debug_remove_old();
}

function debug_warn(msg) {
    console.warn(msg);
    debugmsg.push('Warning:' + msg);
    debug_remove_old();
}

function debug_error(msg) {
    console.error(msg);
    debugmsg.push('Error:' + msg);
    debug_remove_old();
}

function debug_update_page() {
    let elt = document.querySelector('#debugtext');
    if (elt == undefined || elt == null) {
        return;
    }

    elt.innerText = "";
    debugmsg.forEach( (msg) => {
        elt.innerText += msg + "\n";
    });
}
